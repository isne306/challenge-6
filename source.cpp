﻿//project file that demonstrates  fraction class
#include <iostream>
#include "fraction.h"
using namespace std;
int main()
{	
	int num1,num2,denom1,denom2;

	cout << "Enter The Numerator 1: ";
	cin >> num1;
	do 
	{
		cout << "Enter The Denominator 1: ";
		cin >> denom1;
		if (denom1 == 0)
		{
			cout << "Try agin";
		}
		cout << endl;

	}while(denom1 == 0);


	cout << "Enter The Numerator 2: ";
	cin >> num2;

	do 
	{
		cout << "Enter The Denominator 2: ";
		cin >> denom2;
		if (denom2 == 0)
		{
			cout << "Try agin";
		}
		cout << endl;
	}while(denom2 == 0);

	fraction a(num1 ,denom1);
	fraction b(num2 ,denom2);

	a.outputSol();
	cout << " + " ;
	b.outputSol();
	cout << " = " ;

	fraction result1 = a + b;
	result1.outputRe();

	cout << endl;

	a.outputSol();
	cout << " - " ;
	b.outputSol();
	cout << " = " ;

	fraction result2 = a - b;
	result2.outputRe();

	cout << endl;

	a.outputSol();
	cout << " x " ;
	b.outputSol();
	cout << " = " ;

	fraction result3 = a * b;
	result3.outputRe();

	cout << endl;

	a.outputSol();
	cout << " / " ;
	b.outputSol();
	cout << " = " ;

	fraction result4 = a / b;
	result4.outputRe();

	cout << endl;

	a.outputSol();
	cout << " ++ " ;
	cout << " = " ;

	fraction result5 = a++ ;
	result5.outputRe();

	cout << endl;

	a.outputSol();
	cout << " -- " ;
	cout << " = " ;

	fraction result6 = a-- ;
	result6.outputRe();

	cout << endl;

	if(a == b)
	{
		a.outputSol();
		cout << " == " ;
		b.outputSol();
		cout << endl;

	}
	if (a != b)
	{
		a.outputSol();
		cout << " != " ;
		b.outputSol();
		cout << endl;
	}
	if (a > b)
	{
		a.outputSol();
		cout << " > " ;
		b.outputSol();
		cout << endl;
	}
	if (a < b)
	{
		a.outputSol();
		cout << " < " ;
		b.outputSol();
		cout << endl;
	}
	if (a >= b)
	{
		a.outputSol();
		cout << " >= " ;
		b.outputSol();
		cout << endl;
	}
	if (a <= b)
	{
		a.outputSol();
		cout << " <= " ;
		b.outputSol();
		cout << endl;
	}
cout << endl;
}