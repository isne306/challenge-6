﻿//implementation file
#include <iostream>
#include "fraction.h"

using namespace std;

fraction::fraction()
{
	num = 1;
	denom = 1;
}
fraction::fraction(int NUM)
{
	num = NUM;
	denom = 1;
}
fraction::fraction(int NUM ,int DENOM)
{
	num = NUM;
	denom = DENOM;
}
int fraction::getnum ()
{
	return num;
}
void fraction::setnum(int x)
{
	num = x;
}
int fraction::getdenom()
{
	return denom;
}
void fraction::setdenom(int y)
{
	denom = y;
}
//func output result
void fraction::outputRe()
{
	if (num == 0)
	{
		cout << "0";
	}
	else if (num == denom)
	{
		cout << "1";
	}
	else if (denom == 1)
	{
		cout << num;
	}

	else
	{	
		cout << "(" <<num << " / " << denom << ")" ;
	}

}

//func output sol
void fraction::outputSol()
{
		cout << "(" <<num << " / " << denom << ")" ;
}

fraction operator + (fraction a ,fraction b)
{
	fraction c((a.num * b.denom) + (a.denom*b.num),(a.denom * b.denom));
	return c;
}
fraction operator - (fraction a ,fraction b)
{
	fraction c((a.num * b.denom) - (a.denom*b.num),(a.denom * b.denom));
	return c;
}
fraction operator * (fraction a ,fraction b)
{
	fraction c((a.num * b.num),(a.denom * b.denom));
	return c;
}
fraction operator / (fraction a ,fraction b)
{
	fraction c((a.num * b.denom),(a.denom * b.num));
	return c;
}
fraction operator ++ (fraction a,int)
{
	fraction c((a.num + a.denom),a.denom);
	return c;
}
fraction operator -- (fraction a,int)
{
	fraction c((a.num - a.denom),a.denom);
	return c;
}
bool operator == (fraction a ,fraction b)
{
	double result1;
	double result2;
	result1 = a.num / (a.denom*1.0);
	result2 = b.num / (b.denom*1.0);
	if(result1 == result2)
	{
		return true;		
	}
	return false;
}
bool operator != (fraction a ,fraction b)
{
	if(a == b)
	{
		return false;		
	}
	return true;
}
bool operator > (fraction a ,fraction b)
{
	double result1;
	double result2;
	result1 = a.num / (a.denom*1.0);
	result2 = b.num / (b.denom*1.0);
	if(result1 > result2)
	{
		return true;		
	}
	return false;
}
bool operator < (fraction a ,fraction b)
{
	double result1;
	double result2;
	result1 = a.num / (a.denom*1.0);
	result2 = b.num / (b.denom*1.0);
	if(result1 < result2)
	{
		return true;		
	}
	return false;
}
bool operator >= (fraction a ,fraction b)
{
	if(a > b || a == b)
	{
		return true;
	}
	return false;		
}
bool operator <= (fraction a ,fraction b)
{
	if(a < b || a == b)
	{
		return true;
	}
	return false;		
}