﻿// a header file (interface)
#ifndef FRACTION_H
#define FRACTION_H
#include <string>
using namespace std;
class fraction
{
private:
	int num;
	int denom;

public:
	fraction();
	fraction(int NUM);
	fraction(int NUM ,int DENOM);

	int getnum ();
	void setnum(int x);
	int getdenom();
	void setdenom(int y);
	void outputRe();
	void outputSol();

	friend fraction operator + (fraction a ,fraction b);
	friend fraction operator - (fraction a ,fraction b);
	friend fraction operator * (fraction a ,fraction b);
	friend fraction operator / (fraction a ,fraction b);

	friend fraction operator ++ (fraction a,int);
	friend fraction operator -- (fraction a,int);

	friend bool operator == (fraction a ,fraction b);
	friend bool operator != (fraction a ,fraction b);
	friend bool operator > (fraction a ,fraction b);
	friend bool operator < (fraction a ,fraction b);
	friend bool operator >= (fraction a ,fraction b);
	friend bool operator <= (fraction a ,fraction b);
};
#endif //FRACTION_H